package land.shield.xraydetection.listeners;

import land.shield.xraydetection.maps.AlreadyAlertCharacters;
import land.shield.xraydetection.maps.MiningHistoryMap;
import land.shield.xraydetection.service.DebrisDetectorService;
import land.shield.xraydetection.service.DiamondDetectorService;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;


public class BrokeBlockListener implements Listener {
    private final MiningHistoryMap diamondHistoryMap;
    private final MiningHistoryMap debrisHistoryMap;
    private final AlreadyAlertCharacters alertCharacters = new AlreadyAlertCharacters();
    private final FileConfiguration config;

    public BrokeBlockListener(FileConfiguration config, MiningHistoryMap diamondHistoryMap, MiningHistoryMap debrisHistoryMap) {
        this.config = config;
        this.diamondHistoryMap = diamondHistoryMap;
        this.debrisHistoryMap = debrisHistoryMap;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBroken(@NotNull BlockBreakEvent event) {
        Block block = event.getBlock();

        switch (block.getWorld().getEnvironment()) {
            case NORMAL -> {
                try {
                    (new DiamondDetectorService(event, this.alertCharacters, this.diamondHistoryMap, this.config)).detect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            case NETHER -> {
                try {
                    (new DebrisDetectorService(event, this.alertCharacters, this.debrisHistoryMap, this.config)).detect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
