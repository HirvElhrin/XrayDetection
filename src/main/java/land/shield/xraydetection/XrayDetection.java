package land.shield.xraydetection;

import land.shield.xraydetection.listeners.BrokeBlockListener;
import land.shield.xraydetection.maps.MiningHistoryMap;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public final class XrayDetection extends JavaPlugin {
    private final FileConfiguration config = getConfig();
    private final MiningHistoryMap diamondHistoryMap = new MiningHistoryMap();
    private final MiningHistoryMap debrisHistoryMap = new MiningHistoryMap();
    @Override
    public void onEnable() {
        getServer()
                .getPluginManager()
                .registerEvents(new BrokeBlockListener(
                        this.config,
                        this.diamondHistoryMap,
                        this.debrisHistoryMap
                ), this);

        getServer()
                .getScheduler()
                .runTaskTimer(
                        this,
                        this.diamondHistoryMap::cleanOldItems,
                        this.config.getInt("cleaning.delay"),
                        this.config.getInt("cleaning.period")
                );

        getServer()
                .getScheduler()
                .runTaskTimer(
                        this,
                        this.debrisHistoryMap::cleanOldItems,
                        this.config.getInt("cleaning.delay"),
                        this.config.getInt("cleaning.period")
                );
    }

    @Override
    public void onDisable() {
        this.diamondHistoryMap.clearAll();
        this.debrisHistoryMap.clearAll();
    }
}
