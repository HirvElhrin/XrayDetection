package land.shield.xraydetection.models;

public record Position(int x, int y, int z) {
    public String toString() {
        return String.format("x: %s, y: %s, z: %s", this.x, this.y, this.z);
    }
}
