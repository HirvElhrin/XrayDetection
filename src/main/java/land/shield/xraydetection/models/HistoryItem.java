package land.shield.xraydetection.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class HistoryItem {
    private String nickname;
    private LocalDateTime lastTimestamp = LocalDateTime.now();
    private ArrayList<Position> positions = new ArrayList<>();

    public HistoryItem(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return this.nickname;
    }

    public LocalDateTime getLastTimestamp() {
        return this.lastTimestamp;
    }

    public void setNewTimestamp() {
        this.lastTimestamp = LocalDateTime.now();
    }

    public int getPositionsLength() {
        return this.positions.size();
    }

    public ArrayList<String> getPositionsNames() {
        return this.positions
                .stream()
                .map(Position::toString)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public Position getLastPosition() {
        return this.positions.size() == 0 ? null : this.positions.get(this.positions.size() - 1);
    }

    public void setNewPosition(int x, int y, int z) {
        this.positions.add(new Position(x, y, z));
    }

    public void clearPositions() {
        this.positions = new ArrayList<>();
    }
}
