package land.shield.xraydetection.service;

import land.shield.xraydetection.maps.AlreadyAlertCharacters;
import land.shield.xraydetection.maps.MiningHistoryMap;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DebrisDetectorService extends BaseDetectorService {
    public DebrisDetectorService(
            @NotNull BlockBreakEvent event,
            @NotNull AlreadyAlertCharacters alertCharacters,
            @NotNull MiningHistoryMap historyMap,
            @NotNull FileConfiguration config
    ) {
        super(event, alertCharacters, historyMap, config);
    }

    @Override
    protected List<Material> getDetectionBlockList() {
        return List.of(Material.ANCIENT_DEBRIS);
    }

    @Override
    protected int getMaxYLevel() {
        return this.config.getInt("diamonds.level.max");
    }

    @Override
    protected int getMinYLevel() {
        return this.config.getInt("diamonds.level.min");
    }

    @Override
    protected int getOreRadius() {
        return this.config.getInt("diamonds.radius");
    }

    @Override
    protected String getBlockName() {
        return "древние осколки";
    }
}
