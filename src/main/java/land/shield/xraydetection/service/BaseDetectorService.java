package land.shield.xraydetection.service;

import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Guild;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import land.shield.xraydetection.maps.AlreadyAlertCharacters;
import land.shield.xraydetection.maps.MiningHistoryMap;
import land.shield.xraydetection.models.HistoryItem;
import land.shield.xraydetection.models.Position;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

public abstract class BaseDetectorService {
    private final Player player;
    private final Block block;
    private final AlreadyAlertCharacters alertCharacters;
    private final MiningHistoryMap historyMap;
    protected FileConfiguration config;
    public BaseDetectorService(
            @NotNull BlockBreakEvent event,
            @NotNull AlreadyAlertCharacters alertCharacters,
            @NotNull MiningHistoryMap historyMap,
            @NotNull FileConfiguration config
    ) {
        this.player = event.getPlayer();
        this.block = event.getBlock();
        this.alertCharacters = alertCharacters;
        this.historyMap = historyMap;
        this.config = config;
    }

    protected abstract List<Material> getDetectionBlockList();
    protected abstract int getMaxYLevel();
    protected abstract int getMinYLevel();
    protected abstract int getOreRadius();
    protected abstract String getBlockName();

    public void detect() {
        if (block.getY() > this.getMaxYLevel() || block.getY() < this.getMinYLevel()) {
            return;
        }
        if (!this.isBlockIsDetection()) {
            return;
        }
        if (this.isPlayerAlreadyInList()) {
            return;
        }

        HistoryItem historyItem = this.getHistoryItemByPlayerUuid();
        if (Objects.isNull(historyItem)) {
            this.setItemToMap();
            return;
        }

        long timeInterval = this.getTimeInterval(historyItem);

        historyItem.setNewTimestamp();

        if (timeInterval < this.config.getInt("interval.min") || timeInterval > this.config.getInt("interval.max")) {
            historyItem.clearPositions();
            return;
        }

        if (this.isBlockInVein(historyItem)) {
            return;
        }

        this.setNewPosition(historyItem);

        if (historyItem.getPositionsLength() >= 3) {
            this.alertCharacters.addToList(this.player.getUniqueId().toString());
            // Отправляем в дискорд никнейм и список координат
            Bukkit.getLogger().info(this.getDiscordText(historyItem));
            this.sendMessageToDiscord(historyItem);
        }
    }

    private void sendMessageToDiscord(@NotNull HistoryItem historyItem) {
        Guild guild = DiscordSRV.getPlugin().getMainGuild();
        String channelId = this.config.getString("discord.alertChannel");
        if (channelId == null) {
            return;
        }
        TextChannel channel = guild.getTextChannelById(channelId);
        if (channel == null) {
            return;
        }
        channel.sendMessage(this.getDiscordText(historyItem)).queue();
    }

    private String getDiscordText(@NotNull HistoryItem historyItem) {
        return String.format("""
                Персонаж с ником %s заподозрен в читерстве
                Мир - %s
                Измерение - %s
                Блок - %s
                Координаты подозрительных блоков:
                %s
                """,
                this.player.getName(),
                this.block.getWorld().getName(),
                this.block.getWorld().getEnvironment().name(),
                this.getBlockName(),
                String.join("\n", historyItem.getPositionsNames())
        );
    }

    private boolean isBlockIsDetection() {
        return this.getDetectionBlockList().contains(this.block.getType());
    }

    private void setItemToMap() {
        HistoryItem newHistoryItem = (new HistoryItem(this.player.getName()));
        this.setNewPosition(newHistoryItem);

        this.historyMap.setToHistory(
                this.player.getUniqueId().toString(),
                newHistoryItem
        );
    }

    private boolean isPlayerAlreadyInList() {
        return this.alertCharacters.isExistItem(this.player.getUniqueId().toString());
    }

    private void setNewPosition(HistoryItem historyItem) {
        historyItem.setNewPosition(this.block.getX(), this.block.getY(), this.block.getZ());
    }

    private long getTimeInterval(@NotNull HistoryItem historyItem) {
        return historyItem.getLastTimestamp().until(LocalDateTime.now(), ChronoUnit.SECONDS);
    }

    private HistoryItem getHistoryItemByPlayerUuid() {
        return this.historyMap.getItem(this.player.getUniqueId().toString());
    }

    private boolean isBlockInVein(@NotNull HistoryItem historyItem) {
        Position lastPosition = historyItem.getLastPosition();

        if (Objects.isNull(lastPosition)) {
            return false;
        }

        int xDifference = this.block.getX() - lastPosition.x();
        int yDifference = this.block.getY() - lastPosition.y();
        int zDifference = this.block.getZ() - lastPosition.z();

        int coordinateDifference = Math.abs((xDifference * xDifference)
                + (yDifference * yDifference)
                + (zDifference * zDifference));

        return coordinateDifference <= (this.getOreRadius() * this.getOreRadius());
    }
}
