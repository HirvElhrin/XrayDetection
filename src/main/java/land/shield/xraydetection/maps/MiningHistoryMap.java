package land.shield.xraydetection.maps;

import land.shield.xraydetection.models.HistoryItem;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MiningHistoryMap {

    private Map<String, HistoryItem> history = new HashMap<>();

    public void setToHistory(String uuid, HistoryItem item) {
        this.history.put(uuid, item);
    }

    public HistoryItem getItem(String uuid) {
        return this.history.get(uuid);
    }

    public void cleanOldItems() {
        Map<String, HistoryItem> filteredMap = this.history
                .entrySet()
                .stream()
                .filter(item -> item
                        .getValue()
                        .getLastTimestamp()
                        .until(LocalDateTime.now(), ChronoUnit.SECONDS) > 600)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        this.removeItems(new ArrayList<>(filteredMap.keySet()));
    }

    public void removeItems(@NotNull ArrayList<String> uuids) {
        uuids.forEach(this.history::remove);
    }

    public void clearAll() {
        this.history = new HashMap<>();
    }
}
