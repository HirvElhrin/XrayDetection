package land.shield.xraydetection.maps;

import java.util.ArrayList;

public class AlreadyAlertCharacters {
    private ArrayList<String> list = new ArrayList<>();

    public ArrayList<String> getList() {
        return this.list;
    }

    public boolean isExistItem(String uuid) {
        return this.list.contains(uuid);
    }

    public void addToList(String uuid) {
        this.list.add(uuid);
    }

    public void removeAll() {
        this.list = new ArrayList<>();
    }
}
